
public class Student {

    private String emailAddress;
    private int universityId;
    public int monthlyEbookAllowance;
    public int currentlyBorrowedEbooks;

    public Student(String emailAddress, int universityId)
    {
        this.emailAddress = emailAddress;
        this.universityId = universityId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getUniversityId() {
        return universityId;
    }

    public int getMonthlyEbookAllowance() {
        return monthlyEbookAllowance;
    }

    public int getCurrentlyBorrowedEbooks() {
        return currentlyBorrowedEbooks;
    }

    public void setMonthlyEbookAllowance(int monthlyEbookAllowance) {
        this.monthlyEbookAllowance = monthlyEbookAllowance;
    }
}


public class StudentService
{
    public boolean add(String emailAddress, int universityId)
    {
        System.out.println("Log: Start add student with email '{0}'" + emailAddress);

        if (emailAddress != null && !emailAddress.isEmpty())
           return false;

        StudentRepositorySql studentRepository = new StudentRepositorySql();
        if (studentRepository.Exists(emailAddress))
           return false;

        UniversityRepositorySql universityRepository = new UniversityRepositorySql();
        University university = universityRepository.GetById(universityId);

        Student student = new Student(emailAddress, universityId);

        if (university.getLibraryPackage() == LibraryPackage.Standard)
        {
            student.setMonthlyEbookAllowance(10);
        }
        else if (university.getLibraryPackage() == LibraryPackage.Premium)
        {
            student.setMonthlyEbookAllowance(10 * 2);
        }

        studentRepository.add(student);

        System.out.println("Log: End add student with email '{0}'" + emailAddress);

        return true;
    }

    public Iterable<Student> GetStudentsByUniversity()
    {
        //...
        //You don't need to implement this
        throw new UnsupportedOperationException();
    }

    public Iterable<Student> GetStudentsByCurrentlyBorrowedEbooks()
    {
        //...
        //You don't need to implement this
        throw new UnsupportedOperationException();
    }
}



public class University {
    private int id;
    private String name;
    private LibraryPackage libraryPackage;

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public LibraryPackage getLibraryPackage() {
        return libraryPackage;
    }
}
